#!/bin/bash
. /root/.bashrc
/usr/bin/oc project trades >/dev/null 2>&1

output=$(for name in api mysql jenkins activemq;
do

   /usr/bin/oc get pods | /usr/bin/grep $name | /usr/bin/grep "0/1" | /usr/bin/grep Running | /usr/bin/awk "BEGIN{OFS=\",\"}{print \"$name\"}";
done);

for down in $output; do
 oc scale --replicas=0 dc $down
 oc scale --replicas=1 dc $down

done
 

