#!/bin/bash
. /root/.bashrc
echo "id,message"

if ! /usr/bin/oc get pods >/dev/null 2>&1
then
        cp /openshift/config/master/admin.kubeconfig.original /openshift/config/master/admin.kubeconfig
fi
