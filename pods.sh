#!/bin/bash
. /root/.bashrc
/usr/bin/oc project trades >/dev/null 2>&1

echo "name,id,number,status"
output=$(for name in api mysql jenkins activemq;
do

   /usr/bin/oc get pods |   /usr/bin/grep $name |   /usr/bin/grep Running |   /usr/bin/awk "BEGIN{OFS=\",\"}{print \"$name\",\$1,\$2,\$3}"; 
done); 
/usr/bin/echo "$output"



